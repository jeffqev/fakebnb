<!DOCTYPE html>
<html lang="en">
<head>
<title>Elements</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Travelix Project">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="styles/bootstrap4/bootstrap.min.css">
<link href="plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="styles/elements_styles.css">
<link rel="stylesheet" type="text/css" href="styles/elements_responsive.css">
</head>

<body>

<div class="super_container">
	
	<!-- Header -->

	<header class="header">

		<!-- Top Bar -->

		<div class="top_bar">
			<div class="container">
				<div class="row">
					<div class="col d-flex flex-row">
						<div class="phone">FakeBNB</div>
						<div class="social">
							<ul class="social_list">								
								<li class="social_list_item"><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>

								
							</ul>
						</div>
						<div class="user_box ml-auto">
								<div id="helpu" class="user_box_login user_box_link"><a href="help.php">HELP</a></div>
							<div class="user_box_login user_box_link"><a href="#">login</a></div>
							<div class="user_box_register user_box_link"><a href="#">register</a></div>
						</div>
					</div>
				</div>
			</div>		
		</div>
	</header>

	

	<!-- Home -->

	<div class="home">
		<div class="home_background parallax-window" data-parallax="scroll" data-image-src="images/elements_background.jpg"></div>
		<div class="home_content">
			<div class="home_title">HELP</div>
		</div>
	</div>

	<!-- Elements -->

	<div class="elements">
		<div class="container">
			<div class="row">
				<div class="col">
					
					
					<!-- Progress Bars and Accordions -->

					<div class="pbars_accordions">
						<div class="row">
							<div class="col-lg-12">
								<div class="elements_title">FQA</div>
							</div>
						</div>

						<div class="row pbars_accordions_container">

							<!-- FILA 1 -->

							<div class="col-lg-4">

								<!-- Accordions -->
								<div class="elements_accordions">

									<div class="accordion_container">
										<div class="accordion d-flex flex-row align-items-center active"><div>What are the accepted pay methods</div></div>
										<div class="accordion_panel">
											<p>Our site accepts two main pay methods? </p>
											<p> - Credit Cards</p>
											<p> - PayPal</p>
											</p>
										</div>
									</div>

									<div class="accordion_container">
										<div class="accordion d-flex flex-row align-items-center"><div>Is there a maximun number of people?</div></div>
										<div class="accordion_panel">
											<p>There is no limit for people in a room, But we recommend you to choose the best room for the right number of people in order to have a good time</p>
										</div>
									</div>

									

								</div>
							</div>

							<!-- FILA 2 -->

							<div class="col-lg-4">

									<!-- Accordions -->
									<div class="elements_accordions">
	
										<div class="accordion_container">
											<div class="accordion d-flex flex-row align-items-center "><div>Is my data protected?</div></div>
											<div class="accordion_panel">
												<p>All the data you share on our site is just for Fakebnb use and no other external services besides the pay methods are involved </p>
											</div>
										</div>
	
										<div class="accordion_container">
											<div class="accordion d-flex flex-row align-items-center"><div>Are the flying tickets included? </div></div>
											<div class="accordion_panel">
												<p>No they're not.</p>
												<p>Fakebnb is a room rental service, so flying tickets, food and aditional services go to the user's account .</p>
											</div>
										</div>
	
										
									</div>
								</div>
								<!-- FILA 3 -->

							<div class="col-lg-4">

									<!-- Accordions -->
									<div class="elements_accordions">
	
										<div class="accordion_container">
											<div class="accordion d-flex flex-row align-items-center "><div>What happen if I don't travel?</div></div>
											<div class="accordion_panel">
												<p>If you make a reservation and not travel to the destination, you can't ask for a total refund due to our company policies.</p>
											</div>
										</div>
	
										<div class="accordion_container">
											<div class="accordion d-flex flex-row align-items-center"><div>Why the prices vary so often? </div></div>
											<div class="accordion_panel">
												<p>The prices can vary deppending on the time of the year, the season or festivities, so you have to check our site frequently to get the best deals.</p>
											</div>
										</div>
	
									</div>
								</div>

						</div>
					</div>
					
					<!-- Milestones -->

					
					
					
					

				</div>
			</div>
		</div>	
	</div>

	<!-- Footer -->

	
	<!-- Copyright -->

	<div class="copyright">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 order-lg-1 order-2  ">
					<div class="copyright_content d-flex flex-row align-items-center">
						<div><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></div>
					</div>
				</div>
				<div class="col-lg-9 order-lg-2 order-1">
					<div class="footer_nav_container d-flex flex-row align-items-center justify-content-lg-end">
						<div class="footer_nav">
							<ul class="footer_nav_list">
								<li class="footer_nav_item"><a href="index.html">home</a></li>
								<li class="footer_nav_item"><a href="about.html">about us</a></li>
								<li class="footer_nav_item"><a href="offers.html">offers</a></li>
								<li class="footer_nav_item"><a href="blog.html">news</a></li>
								<li class="footer_nav_item"><a href="#">contact</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>

<script src="js/jquery-3.2.1.min.js"></script>
<script src="styles/bootstrap4/popper.js"></script>
<script src="styles/bootstrap4/bootstrap.min.js"></script>
<script src="plugins/greensock/TweenMax.min.js"></script>
<script src="plugins/greensock/TimelineMax.min.js"></script>
<script src="plugins/scrollmagic/ScrollMagic.min.js"></script>
<script src="plugins/greensock/animation.gsap.min.js"></script>
<script src="plugins/greensock/ScrollToPlugin.min.js"></script>
<script src="plugins/progressbar/progressbar.min.js"></script>
<script src="plugins/jquery-circle-progress-1.2.2/circle-progress.js"></script>
<script src="plugins/parallax-js-master/parallax.min.js"></script>
<script src="js/elements_custom.js"></script>
<script src="js/metricas.js"></script>
</body>

</html>