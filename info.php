<?php
//echo $_GET['destino'];

include_once 'Metodos/connection.php';
$con = new Connection();

$stm = $con->get_conn()->prepare("SELECT * FROM habitacion WHERE IDHABITACION =".$_GET['id']);
$stm->execute();

$resultado = $stm->fetchall(PDO::FETCH_NUM);

?>

<script>
    var n = 0;
    var num = 0;
    var cont = 0;
    var ver;
    window.addEventListener('load', mytime, false);
    function mytime() {
        window.setInterval(function () {
            num = n;
            //tiempo pasar de pag a pag
            $('#compra').on('click', function (e) {
                e.preventDefault();
                localStorage.setItem("pasar1", JSON.stringify(num));
                location.href ="pago.php";
            });
            //
            n++;
        }, 1000);
    }
</script>

<script>

localStorage.setItem ("tiempoinfo", 0 );


function guardar(){

    let time = localStorage.getItem("tiempoinfo");
    let masunp = Number(time)+1;
    localStorage.setItem ("tiempoinfo", masunp);

}

setInterval(guardar,1000);
</script>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Single Listing</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Travelix Project">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="styles/bootstrap4/bootstrap.min.css">
<link href="plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="plugins/colorbox/colorbox.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.theme.default.css">
<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/animate.css">
<link rel="stylesheet" type="text/css" href="styles/single_listing_styles.css">
<link rel="stylesheet" type="text/css" href="styles/single_listing_responsive.css">

<!-- MIS CSS -->
<link rel="stylesheet" type="text/css" href="styles/fakebnb.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="styles/elements_styles.css">
<link rel="stylesheet" type="text/css" href="styles/elements_responsive.css">



</head>

<body>

<div class="super_container">
	
	<!-- Header -->

	<header class="header">

		<!-- Top Bar -->

		<div class="top_bar">
			<div class="container">
				<div class="row">
					<div class="col d-flex flex-row">
						<div class="phone">+45 345 3324 56789</div>
						<div class="social">
							<ul class="social_list">
								<li class="social_list_item"><a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
								<li class="social_list_item"><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
								<li class="social_list_item"><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
								<li class="social_list_item"><a href="#"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
								<li class="social_list_item"><a href="#"><i class="fa fa-behance" aria-hidden="true"></i></a></li>
								<li class="social_list_item"><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
							</ul>
						</div>
						<div class="user_box ml-auto">
							<div class="user_box_login user_box_link"><a href="#">login</a></div>
							<div class="user_box_register user_box_link"><a href="#">register</a></div>
						</div>
					</div>
				</div>
			</div>		
		</div>

		<!-- Main Navigation -->

		
	</header>

	

	<!-- Home -->

	

	<!-- Offers -->

	<div class="listing">

		<!-- Search -->

		<div class="search">
			
			
		</div>

		<!-- Single Listing -->

		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="single_listing">
						
						<!-- Hotel Info -->

						<div style="margin-top: 60px;" class="hotel_info">

							<!-- Title -->
							<div class="hotel_title_container d-flex flex-lg-row flex-column">
								<div class="hotel_title_content">
									<h1 class="hotel_title"><?php echo $resultado[0][2];?></h1>
									<div class="rating_r rating_r_<?php echo $resultado[0][20];?> hotel_rating">
										<i></i>
										<i></i>
										<i></i>
										<i></i>
										<i></i>
									</div>
									<div class="hotel_location"><?php echo $resultado[0][6];?></div>
									<div class="offers_price">$<?php echo $resultado[0][11];?><span>per night</span></div>

								</div>
								
							</div>

							<!-- Listing Image -->

							<div class="hotel_image">
								<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
									<ol class="carousel-indicators">
									<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
									<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
									<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
									</ol>
									<div class="carousel-inner">
									<div class="carousel-item active">
										<img class="d-block w-100" src="images/<?php echo $resultado[0][19];?>" alt="First slide">
										
									</div>
									
									<div class="carousel-item">
										<img class="d-block w-100" src="images/<?php echo $resultado[0][19];?>" alt="Second slide">
									</div>
									<div class="carousel-item">
										<img class="d-block w-100" src="images/<?php echo $resultado[0][19];?>" alt="Third slide">
									</div>
									
									</div>
									<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
									<span class="carousel-control-prev-icon" aria-hidden="true"></span>
									<span class="sr-only">Previous</span>
									</a>
									<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
									<span class="carousel-control-next-icon" aria-hidden="true"></span>
									<span class="sr-only">Next</span>
									</a>
								</div>
							</div>

							<!-- Hotel Gallery -->

							

							<!-- Hotel Info Text -->

							<div class="row">
								<div class="col-md-6">
									<div class="hotel_info_text">
									<h2>Servicio Basico</h2>
									<ul>
										<li> Capacidad: <strong><?php echo $resultado[0][7];?></strong> </li>
										<li> Numero de camas: <strong><?php echo $resultado[0][8];?></strong> </li>
										<li> Numero de recamaras: <strong><?php echo $resultado[0][9];?></strong>  </li>
										<li> Numero de Baño: <strong><?php echo $resultado[0][10];?></strong> </li>
									</ul>
									</div>
								</div>
								<div class="col-md-6">
								<div class="hotel_info_text">
								<h2>Servicio Extras</h2>
								<ul>
									<li>
									<?php echo ($resultado[0][13] == 1) ?"Cocina <br>": "";?>
									<?php echo ($resultado[0][14] == 1) ?"Wifi <br>": "";?>	
									<?php echo ($resultado[0][15] == 1) ?"Parqueadero <br>": "";?>
									<?php echo ($resultado[0][16] == 1) ?"Mascotas <br>": "";?>
									<?php echo ($resultado[0][17] == 1) ?"Alberca <br>": "";?>
									<?php echo ($resultado[0][18] == 1) ?"Lavadora <br>": "";?>
									</li>
								</ul>
								</div>
							</div>

							</div>

							

							<!-- Hotel Info Tags -->
							<button id="compra" class="btn btn-success btn-block mt-2"  >
								BOOK!!</button>						

						</div>
						
						
					</div>
				</div>
			</div>
		</div>		
	</div>

	<!-- Footer -->

	

	<!-- Copyright -->

	<div class="copyright">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 order-lg-1 order-2  ">
					<div class="copyright_content d-flex flex-row align-items-center">
						<div><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></div>
					</div>
				</div>
				<div class="col-lg-9 order-lg-2 order-1">
					<div class="footer_nav_container d-flex flex-row align-items-center justify-content-lg-end">
						<div class="footer_nav">
							<ul class="footer_nav_list">
								<li class="footer_nav_item"><a href="index.html">home</a></li>
								<li class="footer_nav_item"><a href="about.html">about us</a></li>
								<li class="footer_nav_item"><a href="offers.html">offers</a></li>
								<li class="footer_nav_item"><a href="blog.html">news</a></li>
								<li class="footer_nav_item"><a href="contact.html">contact</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>

<script src="js/jquery-3.2.1.min.js"></script>
<script src="styles/bootstrap4/popper.js"></script>
<script src="styles/bootstrap4/bootstrap.min.js"></script>
<script src="plugins/easing/easing.js"></script>
<script src="plugins/parallax-js-master/parallax.min.js"></script>
<script src="plugins/colorbox/jquery.colorbox-min.js"></script>
<script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyCIwF204lFZg1y4kPSIhKaHEXMLYxxuMhA"></script>
<script src="js/single_listing_custom.js"></script>

<script src="plugins/greensock/TweenMax.min.js"></script>
<script src="plugins/greensock/TimelineMax.min.js"></script>
<script src="plugins/scrollmagic/ScrollMagic.min.js"></script>
<script src="plugins/greensock/animation.gsap.min.js"></script>
<script src="plugins/greensock/ScrollToPlugin.min.js"></script>
<script src="plugins/progressbar/progressbar.min.js"></script>
<script src="plugins/jquery-circle-progress-1.2.2/circle-progress.js"></script>
<script src="plugins/parallax-js-master/parallax.min.js"></script>
<script src="js/elements_custom.js"></script>


</body>

</html>